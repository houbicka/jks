import re # import package for regex expressions
import json # import package for json format


#get song names
with open('song_names.txt', encoding='utf8') as fp:
    song_names_list = fp.read().split('\n') # split into lines
    names_dictionary = {}
    for song_name in song_names_list: 
        number = song_name.split(" ", 2)[0].strip('.') # split by space twice, take the first element and remove dots
        name = song_name.split(" ", 2)[2] # take the third element
        song_dict = {number:name} # create dictionary
        names_dictionary.update(song_dict)

# create a dictionary for the output
output_json = {"songbook": "JKS", "songs": []}

# open the source data
with open('spevnik_sk_full_v2.txt', encoding='utf8') as fp:
    songs_list = fp.read().split('▲▲▲ <#hore>\n\n') # split into songs by the delimiter
    del songs_list[0] # delete the empty beginning
    
    for song in songs_list:
        
        # create song json
        song_number = song.split('\n')[0].strip('.')
        song_name = names_dictionary.get(re.sub(r"[A-Z]","",song_number))
        song_json = {"id":song_number, "name":song_name, "verses":[]}
        song = song.replace('"', '')

        # first format 
        if song.count('-') > 3:
            verses_list = re.split(r"\n\d+. ", song)
            del verses_list[0]

            if verses_list == []:
                verses_list = re.split(r"\n- \d+. ", song)
                del verses_list[0]
                
            for verse in verses_list:
                verse_output = []
                verse = verse.strip(' -')
                lines = verse.split('-')
                for line in lines:
                    line = line.replace('\n',' ')
                    line = line.strip(' ')
                    verse_output.append(line)
                song_json["verses"].append(verse_output) 
            output_json["songs"].append(song_json)
        
        # second format
        elif '—' in song:
            verses_list = re.split(r"\n— \d+. ", song)
            del verses_list[0]
                
            for verse in verses_list:
                verse_output = []
                verse = verse.strip(' —')
                lines = verse.split('—')
                for line in lines:
                    line = line.replace('\n',' ')
                    line = line.strip(' ')
                    verse_output.append(line)
                song_json["verses"].append(verse_output) 
            output_json["songs"].append(song_json)

        # format without strophes    
        else:
            # replace more than two enters with two enters
            song = re.sub(r'\n\n+', '\n\n', song).strip()
            # use two enters for splitting strophes 
            verses_list = song.split('\n\n')

            for verse in verses_list:
                verse_output = []
                # use one enter for splitting verses
                lines = verse.split('\n')
                for line in lines:
                    line = line.strip(' ') # delete extra spaces 
                    if not(re.search(r"\d+.", line)):  # do not include lines with song number  
                        verse_output.append(line)    
                song_json["verses"].append(verse_output) 
            output_json["songs"].append(song_json)

# generate output json file
with open('OutputSk.json', 'w', encoding='utf8') as fp:
    json.dump(output_json, fp, indent = 2, ensure_ascii=False)

                


