# Jednotny katolicky spevnik (JKS)
## Source
Texts downloaded from the website http://spevnik.szm.com/. The rest of the songs got by the author of this page Tomáš Bacigál in a word document. Both sources merged into one document *spevnik_sk_full_v2.txt*. Song_names saved into another document *song_names.txt*. All the texts were manually corrected (cause it contained many mistakes and typos). The original songbook in pdf was used for corrections. 
A better but not complete source can be found here  https://github.com/stanislavbebej/ejks. The output could be added to this repo in the future (the db downloaded to the repo).

## Python script
### Used packages
The python *re* package was used for working with regex expressions. The second package to be imported is *json* for creating the json output.

### Script

#### Song names
Song names are extracted from the *song_names.txt* file. The file is splitted by enter into a list of songs. Each line is splitted by the second space twice into a number and name and values are put into a dictionary.

#### Songs
The file is splitted by the following string into songs '▲▲▲ <#hore>\n\n'. Then, there are three options to process song texts.